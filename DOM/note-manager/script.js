//ADD NOTE

//select ul listu , zato sto tu hocu da obavljam posao
var ul = document.querySelector('ul');
// console.log(ul);

//ADD EVENT LISTENER ON BUTTON
document.getElementById('add-btn').addEventListener('click', function (e) {
    //nemoj da uradis svoju default funkciju u ovom slucaju to je submit forme u bazu
    e.preventDefault();

    //ovde selektujemo input polje i stavljamo ga u varijablu addInput
    var addInput = document.getElementById('add-input');
    // console.log(addInput);
    if (addInput.value !== '') {
        var li = document.createElement('li'),
            firstPhar = document.createElement('p'),
            secondPhar = document.createElement('p'),
            fristIcon = document.createElement('i'),
            secondIcon = document.createElement('i'),
            input = document.createElement('input');

        fristIcon.className = "fa fa-pencil-square-o";
        secondIcon.className = "fa fa-times";
        input.className = "edit-note";

        input.setAttribute('type', 'text');

        firstPhar.textContent = addInput.value;

        secondPhar.appendChild(fristIcon);
        secondPhar.appendChild(secondIcon);

        li.appendChild(firstPhar);
        li.appendChild(secondPhar);
        li.appendChild(input);

        ul.appendChild(li);

        addInput.value = '';
    } else {
        console.log(typeof addInput.value);
    }

});
//DELETE AND EDIT
ul.addEventListener('click', function (e) {
    if (e.target.classList[1] === 'fa-pencil-square-o') {

        // console.log(e.target.classList);

        var parentPhar = e.target.parentNode;
        // console.log(parentPhar);
        parentPhar.style.display = "none";

        var note = parentPhar.previousElementSibling;
        var input = parentPhar.nextElementSibling;
        input.style.display = "block";

        input.value = note.textContent;

        input.addEventListener('keypress', function (e) {
            if (e.keyCode === 13) {
                if (input.value !== '') {
                    note.textContent = input.value;
                    parentPhar.style.display = "block";
                    input.style.display = "none";
                } else {
                    var li = input.parentNode;
                    li.parentNode.removeChild(li);
                }
            }
        });
    } else if (e.target.classList[1] === 'fa-times') {
        var li = e.target.parentNode.parentNode;
        li.parentNode.removeChild(li);
    }
});

//HIDE NOTES
var hideItems = document.getElementById('hide');
hideItems.addEventListener('click', function () {

    var label = document.querySelector('label');

    if (hideItems.checked) {
        ul.style.display = "none";
        label.textContent = "Unhide notes";
    } else {
        ul.style.display = "block";
        label.textContent = "Hide notes";
    }
});
//SEARCH FILTER
var searchInput = document.querySelector('#search-note input');
// console.log(searchInput);
searchInput.addEventListener('keyup', function (e) {
    var searchChar = e.target.value.toUpperCase();
    console.log(searchChar);

    var notes = ul.getElementsByTagName('li');
    // console.log(notes);
    // console.log(typeof(notes));

    //array , u js postoji i nesto sto se zove matrica 
    //to se pojavljuje u obliku liste ali nema istu formu pa je problem sto ne mozemo da 
    //primenimo naredbe koje se primenjuju na array , izbaci gresku iako je lista ili niz dogadjaja ili necega
    // var liste = [5, 6, 7];
    // for (var i = 0; i < liste.length; i++) {
    //     console.log(i);
    // }

    Array.from(notes).forEach(function (note) {

        //od prvog child elementa od li  a to je p tag ovde uzecemo text i staviti varijablu
        var textInLi = note.firstElementChild.textContent;

        //posto je pretrazivanje case sensitive gledaju se mala i velika slova
        //definisacemo da sve prebacujemo u velika slova sto je user uneo i tako i trazimo

        //ukoliko indexOf(trazeniKarakter) !== -1 to znaci da je uneto nesto u input polje za search 
        if (textInLi.toUpperCase().indexOf(searchChar) !== -1) {
            note.style.display = "block";
        } else {
            note.style.display = "none";
        }
    })

})