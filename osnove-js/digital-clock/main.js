/*
Zadatak podrazumeva da se uradi dinamicki sat koji ce se ispisivati na stranici 
u web browser-u i koji radi bez update , tj.bez refreash 
*/

function digitalClock() {
    //instanca na objekat Date()
    var date = new Date();

    //zadatak kaze da moras ispisati dan sat min i sec
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var sec = date.getSeconds();
    var day = date.getDay();

    //testiranje da li nas kod uopste radi do sada
    // console.log(hours);
    // console.log(minutes);
    // console.log(sec);
    // console.log(day);

    // console.log(typeof (hours));

    //estetika ako je duzina ispisanog stringa razlicita kada je dvocifren ili jednocifren sat min i sec
    if (hours.length < 2) {
        hours = "0" + hours;
    }

    if (minutes.length < 2) {
        minutes = "0" + minutes;
    }

    if (sec.length < 2) {
        sec = "0" + sec;
    }
//getDay return / vraca int od 0-6 pa zato moramo napraviti niz naziva dana u nedelji gde ce on da skonta po i ili brojacu
// od 0-6 koji je dan
    var weekDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
//ono sto treba da se ispise po zadatku
    var clock = weekDays[day] + ' ' + hours + ':' + minutes + ':' + sec;
//debuging
    // console.log(clock);
//ispis na stranicu document
    //manipulation with DOM 
    document.getElementById('clock').innerHTML = clock;
}
//pozivanje funkcije
digitalClock();
//da se update na refreash 
setInterval(digitalClock, 1000);